<?php

use App\Http\Controllers\ImageController;
use Illuminate\Support\Facades\Route;
use Spatie\Browsershot\Browsershot;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get("/image/{card_id}",function ($card_id){

        $file_pointer = public_path()."/$card_id.png";
        unlink($file_pointer);
    }

     Browsershot::url("https://sid.desafrica.com/card/$card_id"))
         ->windowSize(1040, 700)
         ->save("$card_id.png");

     return response()->json(["data" => "$card_id.png"]);
});

Route::post("/process-image-zips/{school}/{imei}",function ($school,$imei){

    $name_of_zipped = $imei . "_" . $school . ".zip";

    if (file_exists(public_path()."/$name_of_zipped")){
        $file_pointer = public_path()."/$name_of_zipped";
        unlink($file_pointer);
    }
    $request = request();
    $all_card_ids = explode(",",$request->get("card_ids"));

    $zip = new \ZipArchive();
    if ($zip->open(public_path()."/$name_of_zipped", \ZipArchive::CREATE) === TRUE) {
        foreach ($all_card_ids as $card_id) {
            $zip->addFile("$card_id.png");
        }

        $zip->close();
    }

    return response()->json(["data" => "$name_of_zipped", "ids"=>$all_card_ids]);
});

//Route::get('/process-image', [ImageController::class, 'getIndex'])
//    ->name('control-center');
